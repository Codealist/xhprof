#!/bin/bash

add-apt-repository ppa:ondrej/php5-5.6
apt-get update




apt-get install -y apache2 mysql-client libapache2-mod-php5 php5 php5-cli php5-mysql php5-curl curl php5-intl php5-xsl git
a2enmod rewrite

cat << EOF > /etc/apache2/sites-available/xhprof.conf
ServerName localhost

<VirtualHost *:80>
  DocumentRoot /var/www/default_html/xhprof_html

  <Directory /var/www/default_html/xhprof_html>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>

</VirtualHost>
EOF

a2ensite xhprof.conf
a2dissite 000-default.conf

cat << EOF > /etc/php5/apache2/conf.d/30-xhprof.ini
date.timezone = "Europe/Kiev"
EOF

service apache2 restart

